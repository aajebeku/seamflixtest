package com.example.ayz4sci.seamflixtest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ayz4sci.seamflixtest.helper.Common;
import com.example.ayz4sci.seamflixtest.helper.DBHandler;
import com.example.ayz4sci.seamflixtest.model.User;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.File;
import java.io.FileInputStream;

public class RegisterUserActivity extends AppCompatActivity {
    private EditText firstName, lastName, otherName, emailEditText;
    private Spinner genderSpinner;
    private Button submitButton;
    private User user;
    private File userFile;
    private String status;
    private File imageFile;
    private File path;
    private ImageView imageView;

    private static final int IMAGE_PICK = 1;
    private static final int IMAGE_CAPTURE = 2;

    private DBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        otherName = (EditText) findViewById(R.id.other_name);
        emailEditText = (EditText) findViewById(R.id.email);
        genderSpinner = (Spinner) findViewById(R.id.gender);

        imageView = (ImageView) findViewById(R.id.picture_view);

        Button takePicture = (Button) findViewById(R.id.picture);
        submitButton = (Button) findViewById(R.id.submit);

        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInfo();
                if (userFile != null) {
                    capturePic();
                }
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        path = getFilesDir();

        dbHandler = new DBHandler(this);

    }

    private void saveInfo() {
        String fName = firstName.getText().toString();
        String lName = lastName.getText().toString();
        String oName = otherName.getText().toString();
        final String email = emailEditText.getText().toString();
        String gender = (String) genderSpinner.getSelectedItem();

        if (!fName.isEmpty() && !lName.isEmpty() && !oName.isEmpty() && !email.isEmpty()) {
            user = new User(fName, lName, oName, email, gender);
            //save user object to db
            dbHandler.addUser(user);
            userFile = Common.writeUserToFile(path, user);
        } else {
            Toast.makeText(this, "Please fill all fields above", Toast.LENGTH_LONG).show();
        }
    }

    private void submit() {
        submitButton.setText(R.string.loading);
        submitButton.setEnabled(false);

        if (userFile != null && imageFile != null) {
            new SendFilesToServer().execute();
        }
    }

    private void capturePic() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data != null && resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK
                || data != null && requestCode == IMAGE_CAPTURE) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            if (bitmap != null) {
                Bitmap bitmapImage = Common.getResizedBitmap(bitmap, 200);
                imageView.setImageBitmap(bitmapImage);
                imageFile = Common.writeImageToFile(bitmapImage, path, user);
                submitButton.setEnabled(true);
            }
        }
    }

    private class SendFilesToServer extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSch jsch = new JSch();
                Session session = jsch.getSession(Common.base64ToString(Constants.user), Constants.host);
                session.setPassword(Common.base64ToString(Constants.password));

                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();

                ChannelSftp channelSftp = (ChannelSftp) channel;
                channelSftp.mkdir(user.getfName());
                channelSftp.cd(user.getfName());

                channelSftp.put(new FileInputStream(userFile), userFile.getName(), ChannelSftp.OVERWRITE);
                channelSftp.put(new FileInputStream(imageFile), imageFile.getName(), ChannelSftp.OVERWRITE);
                status = "Successful";

                channel.disconnect();
                session.disconnect();
            } catch (Exception e) {
                status = "Failed";
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (status.equals("Successful")) {
                firstName.setText("");
                lastName.setText("");
                otherName.setText("");
                emailEditText.setText("");
                submitButton.setEnabled(false);
            } else {
                submitButton.setEnabled(true);
            }
            submitButton.setText(R.string.submit);

            Toast.makeText(RegisterUserActivity.this, status, Toast.LENGTH_LONG).show();
        }
    }
}
