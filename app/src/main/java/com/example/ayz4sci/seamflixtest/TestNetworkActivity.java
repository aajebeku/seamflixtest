package com.example.ayz4sci.seamflixtest;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.ayz4sci.seamflixtest.helper.Common;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class TestNetworkActivity extends AppCompatActivity {

    private TextView indicator;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_network);

        indicator = (TextView) findViewById(R.id.indicator);

        new TestNetworkAsync().execute();
    }

    private class TestNetworkAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSch jsch = new JSch();
                Session session = jsch.getSession(Common.base64ToString(Constants.user), Constants.host);
                session.setPassword(Common.base64ToString(Constants.password));

                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);

                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                isConnected = channel.isConnected();

                channel.disconnect();
                session.disconnect();
            } catch (JSchException e) {
                isConnected = false;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            indicator.setText(R.string.done);
            if (isConnected) {
                indicator.setBackgroundColor(Color.GREEN);
            } else {
                indicator.setBackgroundColor(Color.RED);
            }
        }
    }
}
