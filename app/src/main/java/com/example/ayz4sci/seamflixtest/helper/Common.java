package com.example.ayz4sci.seamflixtest.helper;

import android.graphics.Bitmap;
import android.util.Base64;

import com.example.ayz4sci.seamflixtest.model.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Ayz4sci on 20/05/16.
 */
public class Common {

    public static String base64ToString(String base64String) {
        byte[] valueDecoded = Base64.decode(base64String, Base64.DEFAULT);
        return new String(valueDecoded);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static File writeUserToFile(File path, User user) {
        String fileName = String.format("%s_%s.txt", user.getfName(), user.getlName());
        File file = new File(path, fileName);

        try {
            FileOutputStream stream = new FileOutputStream(file);
            stream.write(fileName.getBytes());
            stream.close();
            return file;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File writeImageToFile(Bitmap bitmap, File path, User user) {
        FileOutputStream out = null;
        try {
            File imageFile = new File(path,
                    String.format("%s_%s.jpg", user.getfName(), user.getlName()));
            out = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            return imageFile;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ignored) {
            }
        }
        return null;
    }
}
