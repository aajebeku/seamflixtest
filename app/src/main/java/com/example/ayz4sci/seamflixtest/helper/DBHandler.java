package com.example.ayz4sci.seamflixtest.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ayz4sci.seamflixtest.model.User;

/**
 * Created by Ayz4sci on 20/05/16.
 */
public class DBHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "usersManager";

    // Users table name
    private static final String TABLE_CONTACTS = "users";

    // Users Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FNAME = "fName";
    private static final String KEY_LNAME = "lName";
    private static final String KEY_ONAME = "oName";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_GENDER = "gender";


    //    fName, lName, oName, email, gender
    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_FNAME + " TEXT," +
                KEY_LNAME + " TEXT," +
                KEY_ONAME + " TEXT," +
                KEY_EMAIL + " TEXT," +
                KEY_GENDER + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    // Adding new user
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FNAME, user.getfName());
        values.put(KEY_LNAME, user.getlName());
        values.put(KEY_ONAME, user.getoName());
        values.put(KEY_GENDER, user.getGender());
        values.put(KEY_EMAIL, user.getEmail());

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }
}
