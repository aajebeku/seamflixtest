package com.example.ayz4sci.seamflixtest.model;

import android.graphics.Bitmap;


/**
 * Created by Ayz4sci on 20/05/16.
 */
public class User {

    private String fName, lName, oName, email, gender;

    public User(String fName, String lName, String oName, String email, String gender) {
        this.fName = fName;
        this.lName = lName;
        this.oName = oName;
        this.email = email;
        this.gender = gender;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getoName() {
        return oName;
    }

    public void setoName(String oName) {
        this.oName = oName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
